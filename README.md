# Engineering

This project is meant to track my personal journey as a software engineer

I use [issues](https://gitlab.com/abrams-career-development/engineering/-/issues) to capture various projects, goals, and progress.

## Career development tracking

[Spreadsheet](https://docs.google.com/spreadsheets/d/1iLBJ5KUUyW8fyhcQTM4dprVvaxDp6CfI697H2Prfh-E/edit?usp=sharing) with examples I've collected of notable accomplishments as an engineer. (This link is currently private to GitLab team members only).

## Noteable feedback

[This doc](https://docs.google.com/document/d/10_ckWJsEqyhFT18PF4Y_o_CVWePFOMErY0oHXs7SUJ4/edit?usp=sharing) contains clips and screenshots of noteable feedback and thanks I've received.

